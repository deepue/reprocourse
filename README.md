# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contain the implementation of the project work of the course Using R for reproducible data analysis at University of Helsinki. The project work is about the Analysis of dietary impact on the plasma concentrations of retinol using Linear Models with the publicly available [dataset](http://lib.stat.cmu.edu/datasets/Plasma_Retinol).

### How do I get set up? ###

Require R software.

### Who do I talk to? ###

You can provide your comments, suggestions to pradeep(dot)eranti(at)aalto(dot)fi.